/****************************************************************************
 *
 * cuda-reverse.cu - Array reversal with CUDA
 *
 * Written in 2017 by Moreno Marzolla <moreno.marzolla(at)unibo.it>
 *
 * To the extent possible under law, the author(s) have dedicated all 
 * copyright and related and neighboring rights to this software to the 
 * public domain worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication
 * along with this software. If not, see 
 * <http://creativecommons.org/publicdomain/zero/1.0/>. 
 *
 * ---------------------------------------------------------------------------
 *
 * Compile with:
 * nvcc cuda-reverse.cu -o cuda-reverse
 *
 * Run with:
 * ./cuda-reverse [len]
 *
 * Example:
 * ./cuda-reverse
 *
 ****************************************************************************/
#include <stdio.h>
#include <math.h>

#define BLKSIZE 1024

/* Reverse in[] into out[] */
__global__ void reverse( int *in, int* out, int n )
{
     const int tid = threadIdx.x;
     const int bid = blockIdx.x;
    
    if((bid*BLKSIZE + tid) < n){
        out[n - 1 - ((bid*BLKSIZE)+tid)] = in[(bid*BLKSIZE)+tid];
    }
}

/* In-place reversal of in[] into itself */
__global__ void inplace_reverse( int *in, int n )
{
    const int tid = threadIdx.x;
    const int bid = blockIdx.x;

    if(((bid*BLKSIZE) + tid) < n/2){
        int tmp = in[n-1-((bid*BLKSIZE)+tid)];
        in[n-1-((bid*BLKSIZE)+tid)] = in[(bid*BLKSIZE)+tid];
        in[(bid*BLKSIZE)+tid] = tmp;
    }
}

void fill( int *x, int n )
{
    int i;
    for (i=0; i<n; i++) {
        x[i] = i;
    }
}

int check( int *x, int n )
{
    int i;
    for (i=0; i<n; i++) {
        if (x[i] != n - 1 - i) {
            fprintf(stderr, "Test FAILED: x[%d]=%d, expected %d\n", i, x[i], n-1-i);
            return 0;
        }
    }
    printf("Test OK\n");
    return 1;
}

int main( int argc, char* argv[] )
{
    int *in, *out;
    int *d_in, *d_out;

    int n, nbk, nbk2;
    const int default_len = 1024*1024;

    if ( argc > 2 ) {
        fprintf(stderr, "\nUsage: %s [len]\n\nReverse an array of \"len\" elements (default %d)\n\n", argv[0], default_len);
        return -1;
    }

    if ( argc > 1 ) {
        n = atoi(argv[1]);
    } else {
        n = default_len;
    }

    const size_t size = n * sizeof(*in);
    nbk = (n%BLKSIZE) + 1;

    /* Allocate and initialize in[] and out[] */
    in = (int*)malloc(size); 
    fill(in, n);
    out = (int*)malloc(size);
    
    cudaMalloc((void**)&d_in, size);
    cudaMalloc((void**)&d_out, size);

    cudaMemcpy(d_in, in, size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_out, out, size, cudaMemcpyHostToDevice);

    /* Reverse */
    printf("Reverse %d elements... ", n);
    reverse<<<nbk,BLKSIZE>>>(d_in, d_out, n);
    cudaMemcpy(out, d_out, size, cudaMemcpyDeviceToHost);    
    check(out, n);
    
    nbk2 = (n/2 % BLKSIZE) +1;
    /* In-place reverse */
    printf("In-place reverse %d elements... ", n);
    inplace_reverse<<<nbk2,BLKSIZE>>>(d_in, n);
    cudaMemcpy(in, d_in, size, cudaMemcpyDeviceToHost);    
    check(in, n);

    /* Cleanup */
    free(in); 
    free(out);
    cudaFree(d_in);
    cudaFree(d_out);
    return 0;
}
