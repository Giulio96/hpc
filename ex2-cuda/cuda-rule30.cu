/****************************************************************************
 *
 * cuda-rule30.cu - Rule30 Cellular Automaton with CUDA
 *
 * Written in 2017 by Moreno Marzolla <moreno.marzolla(at)unibo.it>
 *
 * To the extent possible under law, the author(s) have dedicated all
 * copyright and related and neighboring rights to this software to the
 * public domain worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication
 * along with this software. If not, see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 *
 * --------------------------------------------------------------------------
 *
 * This program implements the "rule 30 CA" as described in
 * https://en.wikipedia.org/wiki/Rule_30 . This program uses the CPU
 * only.
 *
 * Compile with:
 * nvcc cuda-rule30.cu -o cuda-rule30
 *
 * Run with:
 * /cuda-rule30 1024 1024
 *
 ****************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#define BLKSIZE 1024

typedef unsigned char cell_t;

/**
 * Given the current state of the CA, compute the next state.
 * This version does not use ghost cells, and therefore it is
 * necessary to pay attention when computing the indexes of the
 * previous and next cells.
 */

__global__ void rule30( cell_t *cur,cell_t *next, int n )
{
    const int tid = threadIdx.x;
    const int bid = blockIdx.x;

    const int p_b = bid == 0 ? n/BLKSIZE : bid-1;
    const int n_b = bid == n/BLKSIZE ? 0 : bid+1;
	  const int p_c = tid == 0 ? (p_b*BLKSIZE + BLKSIZE-1) : (bid*BLKSIZE + tid -1);
    const int n_c = tid == BLKSIZE-1 ? (n_b*BLKSIZE) : (bid*BLKSIZE+tid+1);

    const cell_t left   = cur[p_c];
    const cell_t center = cur[bid*BLKSIZE + tid];
    const cell_t right  = cur[n_c];
    next[tid + bid*BLKSIZE] = ( left && !center && !right) || (!left && !center &&  right) || (!left &&  center && !right) || (!left &&  center &&  right);
}

/**
 * Initialize the domain; all cells are 0, with the exception of a
 * single cell in the middle of the domain.
 */
void init_domain( cell_t *cur, int n )
{
    int i;
    for (i=0; i<n; i++) {
        cur[i] = 0;
    }
    cur[n/2] = 1;
}

/**
 * Dump the current state of the CA to PBM file |out|.
 */
void dump_state( FILE *out, cell_t *cur, int n )
{
    int i;
    for (i=0; i<n; i++) {
        fprintf(out, "%d ", cur[i]);
    }
    fprintf(out, "\n");
}

int main( int argc, char* argv[] )
{
    const char *outname = "rule30.pbm";
    FILE *out;
    cell_t *cur, *next, *tmp;
    cell_t *d_cur, *d_next;
    int width = 1024, steps = 1024, s;
    int n_blocks;

    if ( argc > 3 ) {
        fprintf(stderr, "Usage: %s [width [steps]]\n", argv[0]);
        return -1;
    }

    if ( argc > 1 ) {
        width = atoi(argv[1]);
    }

    if ( argc > 2 ) {
        steps = atoi(argv[2]);
    }
    n_blocks = (width%BLKSIZE);

    const size_t size = width * sizeof(cell_t);

    /* Allocate space for host copy the cur[] and next[] vectors */
    cur = (cell_t*)malloc(size);
    next = (cell_t*)malloc(size);

    cudaMalloc((void**)&d_cur, size);
    cudaMalloc((void**)&d_next, size);


    /* Create the output file */
    out = fopen(outname, "w");
    if ( !out ) {
        fprintf(stderr, "FATAL: cannot create file \"%s\"\n", outname);
        return -1;
    }
    fprintf(out, "P1\n");
    fprintf(out, "# produced by %s %d %d\n", argv[0], width, steps);
    fprintf(out, "%d %d\n", width, steps);

    /* Initialize the domain */
    init_domain(cur, width);

//    cudaMemcpy(d_cur, cur, size, cudaMemcpyHostToDevice);

    /* Evolve the CA */
    for (s=0; s<steps; s++) {
	     cudaMemcpy(d_cur, cur, size, cudaMemcpyHostToDevice);
	     cudaMemcpy(d_next, next, size, cudaMemcpyHostToDevice);

       rule30<<<n_blocks, BLKSIZE>>>(d_cur, d_next, width);

	     cudaMemcpy(next, d_next, size, cudaMemcpyDeviceToHost);
	     cudaMemcpy(cur, d_cur, size, cudaMemcpyDeviceToHost);

       dump_state(out, cur, width);

      /* swap cur and next */
       tmp = cur;
       cur = next;
       next = tmp;
    }

    fclose(out);
    free(cur);
    free(next);
    cudaFree(d_next);
    cudaFree(d_cur);
    return 0;
}
