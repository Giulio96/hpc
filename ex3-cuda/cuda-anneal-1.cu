/****************************************************************************
 *
 * cuda-anneal.cu - Anneal Cellular Automaton with CUDA
 *
 * Written in 2017 by Moreno Marzolla <moreno.marzolla(at)unibo.it>
 *
 * To the extent possible under law, the author(s) have dedicated all
 * copyright and related and neighboring rights to this software to the
 * public domain worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication
 * along with this software. If not, see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 *
 * --------------------------------------------------------------------------
 *
 * Compile with:
 * nvcc cuda-anneal.cu -o cuda-anneal
 *
 * Run with:
 * ./cuda-anneal [steps [n]]
 *
 * Example:
 * ./cuda-anneal 64
 * produces a file anneal-00064.pbm
 *
 ****************************************************************************/
#include "hpc.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#define BLKSIZE 32

typedef unsigned char cell_t;

/* The following function makes indexing of the two-dimensional CA
   grid easier. Instead of writing, e.g., grid[i][j] (which you can
   not do anyway, since the CA grids are passed around as pointers to
   linear blocks of data), you write IDX(grid, n, i, j) to get a
   pointer to grid[i][j]. This function assumes that the size of the
   CA grid is (n+2)*(n+2), where the first and last rows/columns are
   ghost cells.

   Note the use of both the __device__ and __host__ qualifiers: this
   function can be called both from host and device code. */
__device__ __host__ cell_t* IDX(cell_t *grid, int n, int i, int j)
{
    return (grid + i*(n+2) + j);
}

/*
  |grid| points to a (n+2)*(n+2) block of bytes; this function copies
  the top and bottom (n+2) elements to the opposite halo (see figure
  below).

   0 1              n n+1
   | |              | |
   v v              v v
  +-+----------------+-+
  |Y|YYYYYYYYYYYYYYYY|Y| <- 0
  +-+----------------+-+
  |X|XXXXXXXXXXXXXXXX|X| <- 1
  |\|                |\|
  |\|                |\|
  |\|                |\|
  |\|                |\|
  |Y|YYYYYYYYYYYYYYYY|Y| <- n
  +-+----------------+-+
  |X|XXXXXXXXXXXXXXXX|X| <- n+1
  +-+----------------+-+

*/
__global__ void copy_top_bottom(cell_t *grid, int n)
{
    const int tid = threadIdx.x;
    const int bid = blockIdx.x;

    int my_pos = bid*BLKSIZE + tid;

    if(my_pos > 0 && my_pos < n+1){
      *IDX(grid, n, n+1, my_pos) = *IDX(grid, n, 1, my_pos); /* top to bottom halo */
      *IDX(grid, n, 0, my_pos)= *IDX(grid, n, n, my_pos); /* bottom to top halo */
    }
}

/*
  |grid| points to a (n+2)*(n+2) block of bytes; this function copies
  the left and right (n+2) elements to the opposite halo (see figure
  below).

   0 1              n n+1
   | |              | |
   v v              v v
  +-+----------------+-+
  |Y|X\\\\\\\\\\\\\\Y|X| <- 0
  +-+----------------+-+
  |Y|X              Y|X| <- 1
  |Y|X              Y|X|
  |Y|X              Y|X|
  |Y|X              Y|X|
  |Y|X              Y|X|
  |Y|X              Y|X| <- n
  +-+----------------+-+
  |Y|X\\\\\\\\\\\\\\Y|X| <- n+1
  +-+----------------+-+

 */
__global__ void copy_left_right(cell_t *grid, int n)
{
  const int tid = threadIdx.x;
  const int bid = blockIdx.x;

  int my_pos = bid*BLKSIZE + tid;
        if(my_pos > 0 && my_pos < n+1){
          *IDX(grid, n, my_pos, n+1) = *IDX(grid, n, my_pos, 1); /* left column to right halo */
          *IDX(grid, n, my_pos, 0) = *IDX(grid, n, my_pos, n); /* right column to left halo */
        }
}

/* Compute the |next| grid given the current configuration |cur|.
   Both grids have size (n+2)*(n+2) */
__global__ void step(cell_t *cur, cell_t *next, int n)
{
    const int i = blockIdx.y * blockDim.y + threadIdx.y + 1;
    const int j = blockIdx.x * blockDim.x + threadIdx.x + 1;

    if(i > 0 && i < n+1 && j > 0 && j<n+1){
      const int nbors =
                  *IDX(cur, n, i-1, j-1) + *IDX(cur, n, i-1, j) + *IDX(cur, n, i-1, j+1) +
                  *IDX(cur, n, i, j-1) + *IDX(cur, n, i, j) + *IDX(cur, n, i, j+1) +
                  *IDX(cur, n, i+1, j-1) + *IDX(cur, n, i+1, j) + *IDX(cur, n, i+1, j+1);
              *IDX(next, n, i, j) = (nbors >= 6 || nbors == 4);
    }
}

/* Initialize the current grid |cur| of size (n+2)*(n+2) with alive
   cells with density |p|. */
void init( cell_t *cur, int n, float p )
{
    int i, j;
    for (i=1; i<n+1; i++) {
        for (j=1; j<n+1; j++) {
            *IDX(cur, n, i, j) = (((float)rand())/RAND_MAX < p);
        }
    }
}

/* Write the grid |cur|, of size (n+2)*(n+2), to file |fname| in pbm
   (portable bitmap) format. */
void write_pbm( cell_t *cur, int n, const char* fname )
{
    int i, j;
    FILE *f = fopen(fname, "w");
    if (!f) {
        fprintf(stderr, "FATAL: cannot open file \"%s\" for writing\n", fname);
        abort();
    }
    fprintf(f, "P1\n");
    fprintf(f, "# produced by cuda-anneal.cu\n");
    fprintf(f, "%d %d\n", n, n);
    for (i=1; i<n+1; i++) {
        for (j=1; j<n+1; j++) {
            fprintf(f, "%d ", *IDX(cur, n, i, j));
        }
        fprintf(f, "\n");
    }
    fclose(f);
}

int main( int argc, char* argv[] )
{
    char fname[128];
    cell_t *cur, *next,*tmp;
    cell_t *d_cur, *d_next;
    double tstart, tend;
    int s, nsteps = 64, n = 256;

    if ( argc > 3 ) {
        fprintf(stderr, "Usage: %s [nsteps [n]]\n", argv[0]);
        return -1;
    }

    if ( argc > 1 ) {
        nsteps = atoi(argv[1]);
    }

    if ( argc > 2 ) {
        n = atoi(argv[2]);
        n = (n > 2048 ? 2048 : n);
    }

    const size_t size = (n+2)*(n+2)*sizeof(cell_t);

    cur = (cell_t*)malloc(size);
    assert(cur);
    next = (cell_t*)malloc(size);
    assert(next);

    cudaMalloc((void**)&d_cur, size);
    cudaMalloc((void**)&d_next, size);

    dim3 block(BLKSIZE, BLKSIZE);
    dim3 grid ((n + BLKSIZE - 1)/BLKSIZE , (n + BLKSIZE - 1)/BLKSIZE);

    int n_block = n+2/BLKSIZE + 1;

    init(cur, n, 0.5);


    tstart = hpc_gettime();
    for (s=0; s<nsteps; s++) {
        cudaMemcpy(d_cur, cur,  size, cudaMemcpyHostToDevice);

	      copy_top_bottom<<<n_block, BLKSIZE>>>(d_cur, n);
        copy_left_right<<<n_block, BLKSIZE>>>(d_cur, n);
        step<<<grid,block>>>(d_cur, d_next, n);

        cudaMemcpy(next, d_next, size, cudaMemcpyDeviceToHost);
      //  cur = next;
        tmp = cur;
        cur = next;
        next = tmp;
    }
    tend = hpc_gettime();
    fprintf(stderr, "Execution time %f\n", tend - tstart);
    snprintf(fname, sizeof(fname), "anneal-%05d-1.pbm", s);
    write_pbm(cur, n, fname);
    free(cur);
    free(next);
    cudaFree(d_cur);
    cudaFree(d_next);
    return 0;
}
