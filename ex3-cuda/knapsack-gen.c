/****************************************************************************
 *
 * knapsack-gen.c - Input generator for the knapsack solver
 *
 * Written in 2016, 2017 by Moreno Marzolla <moreno.marzolla(at)unibo.it>
 *
 * To the extent possible under law, the author(s) have dedicated all 
 * copyright and related and neighboring rights to this software to the 
 * public domain worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication
 * along with this software. If not, see 
 * <http://creativecommons.org/publicdomain/zero/1.0/>. 
 *
 * --------------------------------------------------------------------------
 *
 * This program can be used to generate input instances for cuda-knapsack.cu.
 *
 * Compile with:
 * gcc -ansi -Wall -Wpedantic knapsack-gen.c -o knapsack-gen
 * 
 * Run with:
 * ./knapsack-gen C n > data.in
 *
 * where C is the knapsack capacity (positive integer), and n is the number of
 * items (positive integer). 
 *
 * Example:
 * ./knapsack-gen 100 1000 > knap-100-1000.in
 *
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h> /* for time() */

int main( int argc, char* argv[] )
{
    int i, C, n;
    if ( argc != 3 ) {
        fprintf(stderr, "\nUsage:\n\t%s C n > out_file\n\nwhere:\n\tC = knapsack capacity (positive integer)\n\tn = number of items (positive integer)\n\n", argv[0]);
        return -1;
    }
    srand(time(NULL));
    C = atoi(argv[1]); assert(C > 0);
    n = atoi(argv[2]); assert(n > 0);
    printf("%d\n%d\n", C, n);
    for (i=0; i<n; i++) {
        printf("%d %f\n", 1 + rand() % (C/2), ((float)rand())/RAND_MAX * 10.0);
    }
    return 0;
}
